# POWERBIM - Changelog

## [Realese] [1.0.1] - 2023-09-15

### Fixed
- Issues sync with ACC
- Cameras focus in Mapbox
- Several minors bugs fixed

### Changed
- Multiple model folders selection
- Added new Report data capabilities
- Report templates importation improvements
- Creation groups in fusions now they create in children model too



