# POWERBIM API - Changelog

## [Unreleased] [0.0.2] - 2021-04-08

### Fixed
 - report download .txt content



## [Unreleased] [0.0.1] - 2021-03-22

### Fixed
* Exceptions powerbi service report update

### Changed
* Get projects (user & hub admin)
* multiple hub admin users
* clusterWs to Socket io

### Added
* Updown report service
* Project service logic
* Forge Login sync hubs and user
* Tickets 
