# COSTSBILLING - Changelog

## [Unreleased] [0.0.1] - 2021-03-22

### Fixed
* Costs calculations

### Changed
* Wallet by hubId

### Added
* Control panel Powerbim Management
    * Costs Management
    * Subscription Management
    * Wallets
    * Billing Management
    * Tickets control

