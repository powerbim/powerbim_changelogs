# POWERBIM FRONTEND - Changelog

## [Unreleased] [0.1.0] - 2021-04-8

### Fixed
- Videos assets play in mute (youtube - vimeo)
- Cameras timeline improve behaviour
- Mapbox route camera
- Viewer2D resize
- Dimensions buttons behaviour
- 1D Audit fixed crashes


### Changed
- Camera animate
- Generate multiple reports (at change model or fusion -remove spinner loading-)



## [Unreleased] [0.0.1] - 2021-03-20

### Fixed
- Dimensions buttons behaviour
- Iot: Bugs fixed in Expandable component with selected iots
- Audit bugs
- Other bugs

### Changed
- Deisgn improvements
- Snapshots to 2D dimensions
- Forge bar buttons

### Added
* User menu options
    * all users: account details, tutorials, contact
    * superadmin functions: Settings Superadmin
    * User hub admin functions: project manager, usage analytics, billing, tickets, subscription plans
* Single element selection properties extension
* List superadmin users only @bim6d.es
* Subscription plan logic
* User roles permissions logic
* Wallet logic
* Iot icons device name added
* Multiple video players Iot: Youtube, Vimeo & Azure
* Report model selections options: ghost, no*ghost, default selection
* Account details form
* Project Manager functions: 
    * List all hub projects
    * Generate PBIM folders
    * Active dimensions
    * User roles management
* Billing: view and download invoices
* Tickets
